#!/usr/local/bin/bash
set -e

if [ -x /usr/local/bin/pbzip2 ]
then
    BZIP=/usr/local/bin/pbzip2
else
    BZIP=bzip2
fi


ID=$1
ROOT=zroot/jails
VSN=21.4

zfs snapshot ${ROOT}/${ID}@snap1
zfs send ${ROOT}/${ID}@snap1 | ${BZIP} > ${ID}.dataset

SIZE=`ls -l ${ID}.dataset | cut -f 5 -w`
SHA=`sha1 -q ${ID}.dataset`
DATE=`date -u "+%Y-%m-%dT%H:%M:%SZ"`
cat <<EOF > $ID.json
{
  "v": 2,
  "uuid": "${ID}",
  "name": "mc-spigot",
  "version": "${VSN}",
  "type": "jail-dataset",
  "os": "Freebsd",
  "files": [
    {
      "size": ${SIZE},
      "compression": "bzip2",
      "sha1": "${SHA}"
    }
  ],
  "requirements": {
    "architecture": "${ARCH}",
    "networks": [{"name": "net0", "description": "public"}]
  },
  "published_at": "${DATE}",
  "public": true,
  "state": "active",
  "disabled": false
}
EOF

IMG_FILE=/var/imgadm/images/$(echo $ROOT | sed 's/\//-/g')-$ID.json

echo  "{\"zpool\":\"${ROOT}\", \"manifest\":" > $IMG_FILE
cat $ID.json >> $IMG_FILE
echo "}" >> $IMG_FILE


>&2 echo "Jail is ready. Snapshot if needed"
echo $ID

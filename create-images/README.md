# How to create an image

To create an image for FreeBSD just execute prep.sh, this will create a zfs dataset and json manifest that will be
installed in /var/imgadm/images to be able to be used by vmadm.
To host your images just an https server is required with a json file that contains all the images manifest in 
an array, for example :

```json
[
{
  "v": 2,
  "uuid": "60eb298b-bd43-11ef-809b-000c298e56b0",
  "name": "FreeBSD",
  "version": "14.2-RELEASE",
  "type": "jail-dataset",
  "os": "freebsd",
  "files": [
    {
      "size": 275698870,
      "compression": "bzip2",
      "sha1": "f3b19c4cb1a0337e23784ea819355cb9f956e603"
    }
  ],
  "requirements": {
    "architecture": "x86_64",
    "networks": [{"name": "net0", "description": "public"}]
  },
  "published_at": "2024-12-18T13:28:13Z",
  "public": true,
  "state": "active",
  "disabled": false
}
]

```

the file must be called images.

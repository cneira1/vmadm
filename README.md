# Introduction

The goal of this is to provide a fast, efficient utility to manage BSD jails. The CLI is designed to mirror SmartOS's vmadm, and we borrow ideas from other Solaris zone utilities where applicable.

The installation of vmadm [is documented here](http://docs.project-fifo.net/v0.9.3/docs/freebsd#section-add-package-repository).

[![asciicast](https://asciinema.org/a/M8sjN0FC64JPBWZqjKIG5sx2q.png)](https://asciinema.org/a/M8sjN0FC64JPBWZqjKIG5sx2q)

## Design

vmadm describes jails as JSON files. These files are compatible with vmadm's files but represent only a subset of the total options.

Data lives in `/usr/local/etc/vmadm`, being an index file and description file per zone. We do this to mimic the way zomeadm works on Solaris but replaces xml+plaintext with JSON.

Images are ZFS datasets that get cloned for a new jail, both living under a given prefix (that can be defined).

## Prerequirements


### libraries

```bash

pkg install pkgconf openssl ca_root_nss
```

### bridge interface
We need to add bridge0 interface to the `/etc/rc.conf` (`em0` might differ for you)

```bash
# set up a bridge interfaces for jails
cloned_interfaces="bridge0"

# plumb interface em0 into bridge0
ifconfig_bridge0="addm em0"
```


### vnet
In order to have vnet enabled in the kernel, you will probably need to build a new one.

If you have installed the src distribution, the following steps provide a suggested method to rebuild the kernel with new options.
```
cd /usr/src/sys/amd64/conf
cat > FIFOKERNEL <<EOL
include GENERIC
ident FIFOKERNEL

nooptions       SCTP   # Stream Control Transmission Protocol
options         VIMAGE # VNET/Vimage support
options         RACCT  # Resource containers
options         RCTL   # same as above
EOL


cd /usr/src
make -j4 buildkernel KERNCONF=FIFOKERNEL
make -j4 installkernel KERNCONF=FIFOKERNEL
reboot
```

It is also possible to compile a kernel from the current development branch. For that and more, please consult the FreeBSD Handbook.

### rctrl

Rctrl needs to be enabled
```bash
echo kern.racct.enable=1 >> /boot/loader.conf
```

### zfs
We need a dataset for the jails:

```bash
zfs create zroot/jails
```

### reboot

Some of the steps above require a reboot, there is however no reason not just do it once at the very end.

## installation

1. Install rust and cargo: `pkg install rust pkgconf openssl ca_root_nss` (if cargo is not part of the rust package it might be required to install it seperately `pkg install cargo`) 
2. Clone this repository using Git or download it as a Zip archive
3. Build the vmadm binary: `cargo build --release`
4. Copy the executable: `cp target/release/vmadm /usr/local/sbin`
5. Create the jails folder: `mkdir /usr/local/etc/vmadm`
6. Create the images folder: `mkdir -p /var/imgadm/images`
7. Create the main config file: `echo 'pool = "zroot/jails"\n[networks]\nadmin = "bridge0"' > /usr/local/etc/vmadm.toml`
8. Import a dataset using `vmadm images avail` and `vmadm images import`.
9. Create a jail: `cat example.json | vmadm create`

Note if you plan to run linux jails you need to load the linux kernel module: `kldload linux64 linux fdescfs linprocfs linsysfs tmpfs`

The devfs ruleset to used can be adjusted in the `/usr/local/etc/vmadm.toml` by adding `devfs_ruleset = <rule number>`.

## update

If you ran 0.1.0 of the vmadm some path's have changed:

`/etc/vmadm.toml` is now `/usr/local/etc/vmadm.toml`

And

`/etc/jails` is now `/usr/local/etc/vmadm`

Moving those directories and files is all that's required.

## usage
```
vmadm 0.1.0
Heinz N. Gies <heinz@project-fifo.net>
vmadm compatible jail manager

USAGE:
    vmadm [FLAGS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
        --startup
    -V, --version    Prints version information
    -v               Sets the level of verbosity

SUBCOMMANDS:
    console    connects to a jails console
    create     creates a new jail
    delete     deletes a jail
    get        gets a jails configuration
    help       Prints this message or the help of the given subcommand(s)
    images     image subcommands
    info       gets a info for a hardware virtualized vm
    list       lists jails
    reboot     reboot a jail
    start      starts a jail
    stop       stops a jail
    update     updates a jail
```

Travis CI scripts form: https://github.com/japaric/trust

## tricks

Make it feel more SmartOS'ish:

```
alias zlogin vmadm console
alias imgadm vmadm images


Updating a jail resources 


```
vmadm update  a311d074-e1e5-4b9d-ab50-074cccabd354 <<EOF
{ "max_physical_memory": 4000 }
EOF
```
To apply the new limits, the jail must be rebooted.

```

To autocomplete vms uuids add this to your .zshrc 

```
_vmadm()
{
    local cur prev opts base
    COMPREPLY=()
    # NOTE: we don't want the aliases boot,halt,destroy,etc here because we
    # want people to use the 'proper' commands when they can.
    COMMANDS="console create create-snapshot delete delete-snapshot get info list lookup reboot receive reprovision rollback-snapshot send start stop sysrq update validate"
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    if [[  ${prev} == 'boot'
        || ${prev} == 'console'
        || ${prev} == 'delete'
        || ${prev} == 'destroy'
        || ${prev} == 'get'
        || ${prev} == 'halt'
        || ${prev} == 'json'
        || ${prev} == 'reboot'
        || ${prev} == 'reprovision'
        || ${prev} == 'send'
        || ${prev} == 'start'
        || ${prev} == 'stop'
        || ${prev} == 'update'
    ]] && [[ ${COMP_WORDS[COMP_CWORD-2]} == "vmadm" ]]; then  
        vms_uuids=$(ls /usr/local/etc/vmadm/*.json | column -t  | cut -d'/' -f6 |\
            cut -d'.' -f1  | sort | uniq)
	vms_alias=$( for i in /usr/local/etc/vmadm/*.json ; do  cat $i | jq .alias; done;)
	vms_uuids="${vms_alias} ${vms_uuids}"
        COMPREPLY=( $(compgen -W "${vms_uuids}" -- ${cur}) )
        if [[ -z "$COMPREPLY" ]]; then
	   echo "$COMPREPLY";
            _zone_alias
        fi
   fi
   return 0;
}
complete -F _vmadm vmadm
```

## IPV6 

/etc/rc.conf 

```
cat /etc/rc.conf | grep bridge0
cloned_interfaces="bridge0"
create_args_bridge0="inet6 auto_linklocal -ifdisabled addm vtnet0"
ifconfig_bridge0="inet 192.168.10.33/24"
ifconfig_bridge0_ipv6="inet6 accept_rtadv"
```

Example jail.conf for ipv6 
``` 
cat /etc/jail.conf.d/dns01.conf
dns01 {
    path = /jails/containers/${name};
    host.hostname = ${name};

    mount.devfs;
    allow.raw_sockets;
    devfs_ruleset = 5;
    exec.clean;
   
    vnet;
    vnet.interface = "${epair}b";
    $id = "254";
    $ip = "192.168.10.${id}/24";
    $gateway = "192.168.10.1";
    $bridge = "bridge0";
    $epair = "epair${id}";

    exec.prestart += "ifconfig ${epair} create up";
    exec.prestart += "ifconfig ${epair}a up descr jail:${name}";
    exec.prestart += "ifconfig ${bridge} addm ${epair}a up";
    
    exec.start  = "ifconfig ${epair}b inet ${ip} up";
    exec.start += "route add default ${gateway}";
    exec.start += "ifconfig ${epair}b inet6 -ifdisabled";
    exec.start += "ifconfig ${epair}b inet6 accept_rtadv";
    exec.start += "rtsold ${epair}b";
    exec.start += "/bin/sh /etc/rc";
      
    exec.stop = "/bin/sh /etc/rc.shutdown";

    exec.poststop  = "ifconfig ${bridge} deletem ${epair}a";
    exec.poststop += "ifconfig ${epair}a destroy";
}
```




##  Caveats

- For dhcp to work inside a nested jail  we need to patch it https://forums.freebsd.org/threads/dhclient-workings.24386/
  the brand install script for jails has the required files 

```patch
From a7865e98e8427c5bdd2b1959c137ebe74a3994e0 Mon Sep 17 00:00:00 2001
From: neirac <cneirabustos@gmail.com>
Date: Wed, 15 Jan 2025 11:34:26 -0300
Subject: [PATCH] run dhclient in empty jail

---
 sbin/dhclient/dhclient.c | 10 +++++++---
 1 file changed, 7 insertions(+), 3 deletions(-)

diff --git a/sbin/dhclient/dhclient.c b/sbin/dhclient/dhclient.c
index 93988d5ce7a4..c26c22645c15 100644
--- a/sbin/dhclient/dhclient.c
+++ b/sbin/dhclient/dhclient.c
@@ -376,7 +376,7 @@ main(int argc, char *argv[])
        int                      ch, fd, quiet = 0, i = 0;
        int                      pipe_fd[2];
        int                      immediate_daemon = 0;
-       struct passwd           *pw;
+       //struct passwd         *pw;
        pid_t                    otherpid;
        cap_rights_t             rights;

@@ -482,12 +482,13 @@ main(int argc, char *argv[])

        if ((nullfd = open(_PATH_DEVNULL, O_RDWR, 0)) == -1)
                error("cannot open %s: %m", _PATH_DEVNULL);
-
+/*
        if ((pw = getpwnam("_dhcp")) == NULL) {
                warning("no such user: _dhcp, falling back to \"nobody\"");
                if ((pw = getpwnam("nobody")) == NULL)
                        error("no such user: nobody");
        }
+*/

        /*
         * Obtain hostname before entering capability mode - it won't be
@@ -539,8 +540,10 @@ main(int argc, char *argv[])
        setproctitle("%s", ifi->name);

        /* setgroups(2) is not permitted in capability mode. */
+/*
        if (setgroups(1, &pw->pw_gid) != 0)
                error("can't restrict groups: %m");
+*/

        if (caph_enter_casper() < 0)
                error("can't enter capability mode: %m");
@@ -556,10 +559,11 @@ main(int argc, char *argv[])
                if (chdir("/") == -1)
                        error("chdir(\"/\")");
        }
-
+/* make it work in empty jail
        if (setegid(pw->pw_gid) || setgid(pw->pw_gid) ||
            seteuid(pw->pw_uid) || setuid(pw->pw_uid))
                error("can't drop privileges: %m");
+*/

        if (immediate_daemon)
                go_daemon();
--
2.45.2
```

A sample devfs.rules is included in /usr/local/etc/vmadm/
### TODO 
- Add patched versions in /usr/local/lib/brand/jail/tools
## BUGS 
- Creating with a non existing image fails with unable to create snapshot
- invalid UUIDs panic 

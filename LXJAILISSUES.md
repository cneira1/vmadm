# LX Jails issues 

## APT 
* APT-Cache start must be increased
* Installing LGMS 
```bash
: Can not write log (Is /dev/pts mounted?) - posix_openpt (2: No such file or directory)
Setting up postfix (2.11.3-1+deb8u2) ...

Postfix configuration was untouched.  If you need to make changes, edit
/etc/postfix/main.cf (and others) as needed.  To view Postfix configuration
values, see postconf(1).

After modifying main.cf, be sure to run '/etc/init.d/postfix reload'.

Running newaliases
newaliases: warning: inet_protocols: disabling IPv6 name/address support: Protocol not supported
newaliases: fatal: inet_addr_local[getifaddrs]: getifaddrs: Address family not supported by protocol
dpkg: error processing package postfix (--configure):
 subprocess installed post-installation script returned error exit status 75
dpkg: dependency problems prevent configuration of mailutils:
 mailutils depends on default-mta | mail-transport-agent; however:
  Package default-mta is not installed.
  Package mail-transport-agent is not installed.
  Package postfix which provides mail-transport-agent is not configured yet.

dpkg: error processing package mailutils (--configure):
 dependency problems - leaving unconfigured
Processing triggers for libc-bin (2.19-18+deb8u6) ...
Errors were encountered while processing:
 postfix
 mailutils
```
